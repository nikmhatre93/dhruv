var express = require('express')
var app = express()
var bodyParser = require('body-parser')
const cors = require('cors')
var port = 3001

var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200,
}

var routes = require('./routes')

var mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/project', { useNewUrlParser: true })

var db = mongoose.connection;
db.on('error', (err) => console.log(`connection error: ${err}`));
db.once('open', () => console.log('Db connected..'));

/* ----- get all data/stuff of the body (POST) parameters ----- */
app.use(bodyParser.json())
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors(corsOptions))

routes(app)

app.listen(port, () => console.log('http://localhost:3001/'))