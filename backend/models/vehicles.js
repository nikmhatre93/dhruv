let mongoose = require('mongoose')

let vehicle = mongoose.Schema({
    name: { type: String },
    id: { type: String },
    frame: { type: String },
    power_train: { type: String },
    wheels: { type: String },
    file: { type: mongoose.Schema.Types.ObjectId, ref: 'file' }
})

module.exports = mongoose.model("vehicle", vehicle);