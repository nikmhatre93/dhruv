let mongoose = require('mongoose')

let file = mongoose.Schema({
    name: { type: String },
    timestamp: { type: Date, default: Date.now }
})

module.exports = mongoose.model("file", file);