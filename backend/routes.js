var crypto = require('crypto')
var path = require('path')
var multer = require('multer')

var storage = multer.diskStorage({
    destination: function (_req, _file, cb) {
        cb(null, './public/')
    },
    filename: function (_req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            if (err) return cb(err)
            cb(null, `${raw.toString('hex')}${Date.now()}${path.extname(file.originalname)}`)

            // let filename = file.originalname.replace(/\s+/g, '_').replace(/\-/g, '_')
            // cb(null, `${filename}`)
        });
    }
})

var upload = multer({ storage: storage })

var fs = require('fs')
var xml2js = require('xml2js')

var parser = new xml2js.Parser();
// var mongoose = require('mongoose')
// var ObjectId = mongoose.Types.ObjectId

var Vehicle = require('./models/vehicles')
var File = require('./models/files')

const routes = (app) => {

    app.get('/', (req, res) => {
        res.send('Server Running')
    })

    // get file
    app.get("/getfile/:name", (req, res) => {
        let name = req.params.name
        res.sendFile(path.join(__dirname, `./public/${name}`));
    });


    app.get('/vehicles', (req, res) => {
        Vehicle.find({}).sort('-_id').populate('file').exec((err, data) => {
            res.send(data)
        })
    })


    app.post('/file', upload.single('file'), (req, res) => {

        fs.readFile(path.join(__dirname, `./public/${req.file.filename}`), (err, data) => {

            parser.parseString(data, (err, result) => {

                let objects_to_insert = []

                let file_name = req.file.filename
                let file = new File({ name: file_name })
                file.save((err, file_data) => {
                    result.vehicles.vehicle.forEach(element => {

                        let id = element.id[0]
                        let name = ''
                        let frame = element.frame[0].material[0]
                        let wheels = 0

                        try {
                            wheels = element.wheels[0].wheel.length
                        } catch (error) { }

                        let power_train = Object.keys(element.powertrain[0])[0]


                        if (frame == 'plastic' && wheels == 3 && power_train == 'human') {
                            name = 'Big Wheel'
                        }
                        else if (frame == 'metal' && wheels == 2 && power_train == 'human') {
                            name = 'Bicycle'
                        }
                        else if (frame == 'metal' && wheels == 2 && power_train == 'internal combustion') {
                            name = 'Motorcycle'
                        }
                        else if (frame == 'metal' && wheels == 4 && power_train == 'internal combustion') {
                            name = 'Car'
                        }
                        else {
                            name = 'Hang Glider'
                        }


                        objects_to_insert.push({
                            id,
                            name,
                            frame,
                            power_train,
                            wheels,
                            file: file_data._id
                        })

                    })


                    Vehicle.insertMany(objects_to_insert).then((err) => {
                        res.send({ error: false, data: objects_to_insert })
                    })
                })



            })
        })

    })

}

module.exports = routes