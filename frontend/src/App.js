import React, { Component } from 'react';
import FileUpload from './FileUpload'
import DataTable from './DataTable'
import Container from 'react-bootstrap/Container'
import axios from 'axios'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: [],
      new_data: []
    }
  }

  componentDidMount = () => {
    this.getData()
  }

  getData = () => {
    axios.get('http://localhost:3001/vehicles')
      .then(res => {
        this.setState({ data: res.data })
      })
  }

  onChange = (e) => {
    let formData = new FormData()
    formData.append('file', e.target.files[0])

    axios({
      method: 'post',
      url: 'http://localhost:3001/file',
      data: formData,
      config: { headers: { 'Content-Type': 'multipart/form-data' } }
    }).then(res => {
      alert('File has been uploaded successfully')
      this.setState({ new_data: res.data.data })
      this.getData()
    })
  }



  render() {
    return (
      <div style={{ marginTop: 35 }}>
        <Container>
          <Tabs defaultActiveKey="fileUpload">
            <Tab eventKey="fileUpload" title="File Upload">
              <div style={{ margin: 30 }}>
                <FileUpload onChange={this.onChange} />
                <br />
                <br />
                <DataTable data={this.state.new_data} file={false} />
              </div>
            </Tab>
            <Tab eventKey="reports" title="Reports">
              <div style={{ margin: 30 }}>
                <DataTable data={this.state.data} file={true} />
              </div>
            </Tab>
          </Tabs>
        </Container>
      </div>
    );
  }
}

export default App;
