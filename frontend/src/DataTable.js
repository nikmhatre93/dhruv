import React from 'react'
import Table from 'react-bootstrap/Table'
import moment from 'moment'

class DataTable extends React.Component {

    renderTable = (data) => {

        return data.map(item => (
            <tr key={item._id}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.frame}</td>
                <td>{item.power_train}</td>
                <td>{item.wheels}</td>
                {
                    this.props.file &&
                    <td><a rel="noopener noreferrer" target="_blank" href={`http://localhost:3001/getfile/${item.file.name}`}>File</a></td>
                }
                {
                    this.props.file &&
                    <td>{moment(item.file.timestamp).format('lll')}</td>
                }
            </tr>
        ))
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.props.data.length > 0 && <Table responsive>
                        <thead>
                            <tr>

                                <th>Id</th>
                                <th>Name</th>
                                <th>Frame</th>
                                <th>Power Train</th>
                                <th>Wheels</th>
                                {this.props.file && <th>File</th>}
                                {this.props.file && <th>Timestamp</th>}
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderTable(this.props.data)}
                        </tbody>
                    </Table>
                }
            </React.Fragment>
        )
    }
}


export default DataTable