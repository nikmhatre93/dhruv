import React from 'react'
import Form from 'react-bootstrap/Form'

class FileUpload extends React.Component {
    render() {
        return (
            <Form.Control type="file" placeholder="Enter email" onChange={this.props.onChange} />
        )
    }
}

export default FileUpload